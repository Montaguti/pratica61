
import utfpr.ct.dainf.if62c.pratica.*;
/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        
        Time time1 = new Time();
        Time time2 = new Time();
        
        Jogador g1 = new Jogador(1, "Tafarel");
        Jogador g2 = new Jogador(12, "Pato");
        
        Jogador z1 = new Jogador(2, "Puyol");
        Jogador z2 = new Jogador(4, "Indio");
        
        time1.addJogador("Goleiro", g1);
        time1.addJogador("Zagueiro", z1);
        time2.addJogador("Goleiro", g2);
        time2.addJogador("Zagueiro", z2);
        
        System.out.println("Posição\tTime 1\tTime 2");
        for (String s: time1.getJogadores().keySet()) {
            System.out.println(s + "\t" + time1.getJogadores().get(s).toString() + "\t" + time2.getJogadores().get(s).toString());
        }
        
    }
}
